import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Tanggal {
    public static void main(String[] args) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date Tanggal1 = sdf.parse("2020-10-10");
        Date Tanggal2 = sdf.parse("2020-04-30");

        System.out.println("Tanggal1 : " + sdf.format(Tanggal1));
        System.out.println("Tanggal2 : " + sdf.format(Tanggal2));

        if (Tanggal1.compareTo(Tanggal2) > 0) {
            System.out.println("Tanggal1 Merupakan Setelah Tanggal2");
        } else if (Tanggal1.compareTo(Tanggal2) < 0) {
            System.out.println("Tanggal1 Sebelum Tanggal2");
        } else if (Tanggal1.compareTo(Tanggal2) == 0) {
            System.out.println("Tanggal1 == Tanggal2");
        } 
    }
}
