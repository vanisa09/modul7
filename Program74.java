import java.util.Random;
import java.util.Scanner;

public class Program74 {

	String[] questions = new String[] {"Ada berapa banyak jari dalam satu tangan?",
			"Apa jari yang ke 2?", 
			"Tangan Manusia Mempunyai 2 tangan yaitu kanan dan?",
			"Berapa Jumlah Kaki Manusia?",
			"Lima Tambah dua sama dengan?"};
	String[] answers = new String[] {"5", "Telunjuk", "Kiri", "2", "Tujuh"};
	int randNumber;
	
	Scanner scan = new Scanner(System.in);
	Random rand = new Random();
	
	public Program74() {
		randNumber = rand.nextInt(5);
		System.out.println(questions[randNumber]);
		System.out.print("Jawaban : ");
		String answer = scan.nextLine();
		
		if(answer.equalsIgnoreCase(answers[randNumber])) {
			System.out.println("Jawaban anda benar");
		} else {
			System.out.println("Jawaban anda salah");
		}
	}
	
	public static void main(String[] args) {
		new Program74();
	}

}
