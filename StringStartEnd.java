public class StringStartEnd {
public static void main(String[] args) {
		String[] strings={"started", "starting", "ended", "ending"};
		for(String string : strings) {
			System.out.printf("\"%s\"starts with\"st\"\n", string);
		}
		System.out.println();
		for(String string : strings) {
			if(string.startsWith("art", 2)) {
				System.out.printf("\"%s\" starts with \"art\"at position 2\n", string);
			}
			System.out.println();
			for(String stringh : strings) {
				if(stringh.endsWith("ed")){
					System.out.printf("\"%s\" ends with \"ed\"\n", string);
				}
			}
		}
	}

}
