import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class Duplikat {
	
	public static void main(String[] args) {
		String a = "Nama Vanisa Latifah Septiani Nama Panggilan Vanisa";

		List<String> list = Arrays.asList(a.split(" "));

		Set<String> uniqueWords = new HashSet<String>(list);
		for (String word : uniqueWords) {
			System.out.println(word + ": " + Collections.frequency(list, word));
		}
	}
}
